﻿[System.Serializable]
public class MarkerInfo 
{
    private string title;
    private string author;
    private string description;
    private string key;
    private string url;

    public string Title{ get => title; }
    public string Author{ get => author; }
    public string Description{ get => description; }
    public string Key { get => key; }
    public string Url { get => url; }
    public MarkerInfo(string title, string description, string key, string url, string author = null)
    {
        this.title = title;
        this.author = author;
        this.description = description;
        this.key = key;
        this.url = url;
    }

    public override string ToString()
    {
        return ($"title = {this.title}\nauthor = {this.author}\ndescription = {this.description}\nkey = {this.key}\n");
    }
}