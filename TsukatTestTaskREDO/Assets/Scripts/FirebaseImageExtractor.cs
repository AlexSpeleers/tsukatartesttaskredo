﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Storage;
using System.Threading.Tasks;
using System.IO;

public class FirebaseImageExtractor : MonoBehaviour
{
    private const long MAXALLOWSIZE = 1 * 1024 * 1024;
    private string imageDirectory;

    private StorageReference storage_ref;
    [SerializeField] private ImageTargetSpawner imageTargetSpawner;
    private void Awake()
    {
        imageDirectory = $"{Application.persistentDataPath}/ImageResourses";
        FirebaseMarkerInfoExtractor.OnMarkerDataLoaded += UploadImagesFromFireBase;
    }

    public void UploadImagesFromFireBase(Dictionary<string, MarkerInfo> resources)
    {
        if (resources.Count > 0)
        {
            storage_ref = FirebaseStorage.DefaultInstance.GetReferenceFromUrl("gs://tsukattesttaskredo.appspot.com");
            StartCoroutine(DownloadImagesRoutine(resources));
        }
    }

    IEnumerator DownloadImagesRoutine(Dictionary<string, MarkerInfo> resources)
    {
        if (!Directory.Exists(imageDirectory) || Directory.GetFiles(imageDirectory, "*", SearchOption.TopDirectoryOnly).Length != resources.Count)
        {
            Debug.Log("inside");
            DirectoryInfo di = Directory.CreateDirectory(imageDirectory);
            foreach (var item in resources)
            {
                storage_ref = FirebaseStorage.DefaultInstance.GetReferenceFromUrl(item.Value.Url); 
                storage_ref.GetBytesAsync(MAXALLOWSIZE).ContinueWith((Task<byte[]> task) =>
                {
                    if (task.IsFaulted || task.IsCanceled)
                    {
                        Debug.Log(task.Exception.ToString());
                    }
                    else
                    {
                        byte[] fileContents = task.Result;
                        File.WriteAllBytes($"{imageDirectory}/{item.Key}.jpg", fileContents);
                    }
                });
            }
        }
        imageTargetSpawner.SpawnTargets(resources, imageDirectory);
        yield return null;
    }

    private void OnApplicationQuit()
    {
        if (Directory.Exists(imageDirectory))
            Directory.Delete(imageDirectory, true);
        FirebaseMarkerInfoExtractor.OnMarkerDataLoaded = null;
    }
}