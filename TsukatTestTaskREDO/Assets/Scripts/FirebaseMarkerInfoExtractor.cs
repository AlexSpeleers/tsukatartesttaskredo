﻿using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Newtonsoft.Json;
using System;

public class FirebaseMarkerInfoExtractor : MonoBehaviour
{
    public static Action<Dictionary<string, MarkerInfo>> OnMarkerDataLoaded;
    private Dictionary<string, MarkerInfo> markerInfos = new Dictionary<string, MarkerInfo>();
    private readonly static Queue<Action<Dictionary<string, MarkerInfo>>> _executionQueue = new Queue<Action<Dictionary<string, MarkerInfo>>>();
    public Dictionary<string, MarkerInfo> MarkerInfos { get => markerInfos; }
    public bool IsInfoRecieved { get; private set; } = false;

    private void Awake()
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tsukattesttaskredo.firebaseio.com/");
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    private void Start()
    {
        LoadMarkerData();
    }

    public void Update()
    {
        while (_executionQueue.Count > 0)
        {
            _executionQueue.Dequeue().Invoke(markerInfos);
            lock (_executionQueue)
            {
                while (_executionQueue.Count > 0)
                {
                    _executionQueue.Dequeue().Invoke(markerInfos);
                }
            }
        }
    }

    private void LoadMarkerData()
    {
        FirebaseDatabase.DefaultInstance.GetReference("Markers")
        .GetValueAsync()
        .ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                IsInfoRecieved = false;
                LoadMarkerData();
                Debug.LogWarning("No connection with database");
                // TODO: Show up panel with connection warning and button to run method again - ¯\_(ツ)_/¯
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                foreach (var item in snapshot.Children)
                {
                    MarkerInfo mi = JsonConvert.DeserializeObject<MarkerInfo>(item.GetRawJsonValue());                    
                    if (mi != null)
                    {
                        markerInfos.Add(mi.Key, mi);                        
                    }
                }
                IsInfoRecieved = true;
                if (OnMarkerDataLoaded != null)                
                    _executionQueue.Enqueue(OnMarkerDataLoaded);
            }
        });
    }
}