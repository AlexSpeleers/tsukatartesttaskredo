﻿using UnityEngine;
using easyar;

public class ImageTarget : MonoBehaviour
{
    [SerializeField] private ImageTargetController imageTargetController;
    [SerializeField] private Panel panel;
    public ImageTargetController ImageTargetController { get => imageTargetController; }
    public Panel Panel { get => panel; }
}