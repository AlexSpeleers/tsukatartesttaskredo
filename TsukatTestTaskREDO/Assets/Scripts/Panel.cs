﻿using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour
{
    [SerializeField] private Text title;
    [SerializeField] private Text author;
    [SerializeField] private Text description;
    public void Merge(MarkerInfo markerInfo)
    {
        title.text = markerInfo.Title;
        author.text = markerInfo.Author;
        description.text = markerInfo.Description;
    }
}