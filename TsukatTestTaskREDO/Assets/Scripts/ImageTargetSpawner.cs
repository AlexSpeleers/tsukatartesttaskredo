﻿using System.Collections.Generic;
using UnityEngine;
using easyar;

public class ImageTargetSpawner : MonoBehaviour
{
    [SerializeField] private ImageTrackerFrameFilter imageTracker;
    [SerializeField] private ImageTarget imageTarget;
    private Dictionary<ImageTargetController, bool> imageTargetControllers = new Dictionary<ImageTargetController, bool>();

    public void SpawnTargets(Dictionary<string, MarkerInfo> resources, string path)
    {
        foreach (var item in resources)
        {
            var obj = Instantiate(imageTarget, this.transform);
            obj.Panel.Merge(item.Value);
            obj.ImageTargetController.Tracker = imageTracker;
            obj.ImageTargetController.ImageFileSource.Path = $"{path}/{item.Key}.jpg";
            obj.ImageTargetController.ImageFileSource.Name = $"{item.Key}";
            imageTargetControllers.Add(obj.ImageTargetController, false);
            AddTargetControllerEvents(obj.ImageTargetController);
        }
    }
    private void AddTargetControllerEvents(ImageTargetController controller)
    {
        if (!controller)
        {
            return;
        }

        controller.TargetFound += () =>
        {
            Debug.LogFormat("Found target {{id = {0}, name = {1}}}", controller.Target.runtimeID(), controller.Target.name());
        };
        controller.TargetLost += () =>
        {
            Debug.LogFormat("Lost target {{id = {0}, name = {1}}}", controller.Target.runtimeID(), controller.Target.name());
        };
        controller.TargetLoad += (Target target, bool status) =>
        {
            imageTargetControllers[controller] = status ? true : imageTargetControllers[controller];
            Debug.LogFormat("Load target {{id = {0}, name = {1}, size = {2}}} into {3} => {4}", target.runtimeID(), target.name(), controller.Size, controller.Tracker.name, status);
        };
        controller.TargetUnload += (Target target, bool status) =>
        {
            imageTargetControllers[controller] = status ? false : imageTargetControllers[controller];
            Debug.LogFormat("Unload target {{id = {0}, name = {1}}} => {2}", target.runtimeID(), target.name(), status);
        };
    }
}